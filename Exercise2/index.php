<?php

function generateRandomArrayASCII(): array
{
    $fromOrd = ord(',');
    $toOrd = ord('|');

    $charactersArray = array();
    for ($charInt = $fromOrd; $charInt <= $toOrd; $charInt++) {
        $charactersArray[] = chr($charInt);
    }
    shuffle($charactersArray);

    return $charactersArray;
}

function findMissingASCIICharacterFromArray(array $array): string
{
    $sumCharactersArray = 0;
    foreach ($array as $character) {
        $sumCharactersArray += ord($character);
    }

    $sumCharactersOrigin = 0;
    for ($characterNumber = ord(','); $characterNumber <= ord('|'); $characterNumber++) {
        $sumCharactersOrigin += $characterNumber;
    }

    return chr($sumCharactersOrigin - $sumCharactersArray);
}

function runExercise()
{
    $charactersArray = generateRandomArrayASCII();

    $removedCharacter = array_pop($charactersArray);

    $missingCharacter = findMissingASCIICharacterFromArray($charactersArray);

    echo "Removed Character: " . $removedCharacter . "<br/>";
    echo "Missing Character: " . $missingCharacter;
}

runExercise();