<?php

namespace Exercise4;

include "ABTest.php";

echo "<pre>";
$x = new ABTest();
$design = $x->getDesign(1); // Generated on first load, Cache on subsequent for 30s
print_r($design);

$design = $x->getDesign(2); // Generated on first load, Cache on subsequent for 30s
print_r($design);

$design = $x->getDesign(3); // Generated on first load, Cache on subsequent for 30s
print_r($design);

echo "</pre>";