<?php

namespace Exercise4;

require_once('../vendor/autoload.php');

use Exads\ABTestData;
use Exads\ABTestException;

class ABTest
{
    /**
     * @throws ABTestException
     */
    public function getDesign(int $promoId): array
    {
        $abTest = new ABTestData($promoId);

        if (isset($_COOKIE["exads_ab_{$promoId}"])) {
            $designId = (int)$_COOKIE["exads_ab_{$promoId}"];
            $design = $abTest->getDesign($designId);
            $design['origin'] = 'cache';
        } else {
            $designs = $abTest->getAllDesigns();

            mt_srand();
            $rand_range = mt_rand(1, 100);

            $designIndex = -1;
            while ($rand_range > 0) {
                $design = $designs[++$designIndex];
                $rand_range -= $design['splitPercent'];
            }

            // Set expire time to 30 seconds (just for testing). AB Tests can vary from a couple of weeks to months.
            $cookieExpireTime = time() + 20;
            setcookie("exads_ab_{$promoId}", $design['designId'], $cookieExpireTime);
            $design['origin'] = 'generated';
        }

        return $design;
    }
}