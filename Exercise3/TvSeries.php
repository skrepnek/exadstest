<?php

namespace Exercise3;

include 'Database.php';
include 'TvSeriesInterval.php';

class TvSeries
{
    public string $title;
    public string $channel;
    public string $genre;
    public TvSeriesInterval $tvSeriesInterval;

    public function __construct(string $title, string $channel, string $genre, int $week_day, string $show_time)
    {
        $this->title = $title;
        $this->channel = $channel;
        $this->genre = $genre;
        $this->tvSeriesInterval = new TvSeriesInterval($week_day, $show_time);
    }

    public static function getNextTvSeries(
        \DateTime $dateTimeReference = null,
        string $titleSearch = null
    ): ?TvSeries {
        if (is_null($dateTimeReference)) {
            $dateTimeReference = new \DateTime();
        }

        $db = new Database();
        $tvShowDate = $dateTimeReference->format('Y-m-d');
        $tvShowTime = $dateTimeReference->format('H:i:s');
        $titleFilter = '';
        if ($titleSearch) {
            $titleFilter = 'WHERE ts.title LIKE :title_filter_value';
            $titleFilterValue = "%{$titleSearch}%";
        }

        $statement_query = "SELECT ts.title, ts.channel, ts.genre, tsi.week_day, tsi.show_time
            FROM tv_series ts
                JOIN tv_series_interval tsi ON ts.id = tsi.id_tv_series
            {$titleFilter}
            ORDER BY tsi.week_day - WEEKDAY(:tv_show_date ) + IF(tsi.week_day = WEEKDAY(:tv_show_date) AND show_time <= :tv_show_time, 7, 0), tsi.show_time
            LIMIT 1";

        $statement = $db->prepare($statement_query);
        $statement->bindParam(':tv_show_date', $tvShowDate);
        $statement->bindParam(':tv_show_time', $tvShowTime);
        if ($titleSearch) {
            $statement->bindParam(':title_filter_value', $titleFilterValue);
        }

        $statement->execute();

        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }

        return new TvSeries(...$result);
    }
}