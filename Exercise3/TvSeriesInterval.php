<?php

namespace Exercise3;

class TvSeriesInterval
{
    public string $dayOfTheWeek;
    public string $showTime;

    public function __construct(int $weekDay, string $showTime)
    {
        $this->dayOfTheWeek = jddayofweek($weekDay, 1);
        $this->showTime = $showTime;
    }
}