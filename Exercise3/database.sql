CREATE DATABASE IF NOT EXISTS `exads_ex3`;
USE `exads_ex3`;

CREATE TABLE IF NOT EXISTS `tv_series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `channel` varchar(64) DEFAULT NULL,
  `genre` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `tv_series` (`id`, `title`, `channel`, `genre`) VALUES
	(1, 'The Walking Dead', 'AMC', 'Horror'),
	(2, 'La Casa De Papel', 'Netflix', 'Drama'),
	(3, 'Star Trek: Discovery', 'Discovery', 'Sci-fy');

CREATE TABLE IF NOT EXISTS `tv_series_interval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tv_series` int(11) DEFAULT NULL,
  `week_day` int(11) DEFAULT NULL,
  `show_time` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tv_series_interval` (`id_tv_series`),
  CONSTRAINT `fk_tv_series_interval` FOREIGN KEY (`id_tv_series`) REFERENCES `tv_series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `tv_series_interval` (`id`, `id_tv_series`, `week_day`, `show_time`) VALUES
	(1, 1, 5, '19:00:00'),
	(2, 2, 2, '20:00:00'),
	(3, 3, 3, '16:00:00');
