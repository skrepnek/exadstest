<?php

include "TvSeries.php";

/*
 * Tv Series provided in db.sql are
 * Title                    Weekday             Show Time
 * La Casa De Papel         Wednesday           20:00
 * Star Trek: Discovery     Thursday            16:00
 * The Walking Dead         Saturday            19:00
 */

print "<pre>";
// Wednesday at 21:00. Should display Star Trek: Discovery
$nextTvSeries = Exercise3\TvSeries::getNextTvSeries(dateTimeReference: new DateTime('2022-05-04 21:00'));
print_r($nextTvSeries);

// Wednesday at 21:00 with filter 'casa'. Should display La Casa De Papel
$nextTvSeries = Exercise3\TvSeries::getNextTvSeries(
    dateTimeReference: new DateTime('2022-05-04 21:00'),
    titleSearch: 'casa'
);
print_r($nextTvSeries);

// Current date time with filters 'dead'. Should display The Walking Dead
$nextTvSeries = Exercise3\TvSeries::getNextTvSeries(titleSearch: 'dead');
print_r($nextTvSeries);

// Current date time without filters. Should display based on the list of the top comment if no query has been changed
$nextTvSeries = Exercise3\TvSeries::getNextTvSeries();
print_r($nextTvSeries);
print "</pre>";

