<?php

namespace Exercise3;

class Database extends \PDO
{
    public function __construct()
    {
        // This can be upgraded to read and retrieve data from a .env or .ini file for example
        $host = 'localhost';
        $dbname = 'exads_ex3';
        $user = 'root';
        $password = '';

        parent::__construct('mysql:host=' . $host . ';dbname=' . $dbname, $user, $password);
    }
}