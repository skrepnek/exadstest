<?php

function printNumbersAndFactors()
{
    for ($number = 1; $number <= 100; $number++) {
        $maxPossibleFactorDivision = (int)sqrt($number);
        $existingFactors = array();
        for ($factor = 1; $factor <= $maxPossibleFactorDivision; $factor++) {
            if ($number % $factor === 0) {
                $existingFactors[] = $factor;
                $existingFactors[] = $number / $factor;
            }
        }
        $existingFactors = array_unique($existingFactors);
        sort($existingFactors);
        if (count($existingFactors) <= 2) {
            echo "{$number}: [PRIME]<br/>";
        } else {
            echo "{$number}: [" . trim(implode(", ", $existingFactors), ", ") . "]<br/>";
        }
    }
}

printNumbersAndFactors();